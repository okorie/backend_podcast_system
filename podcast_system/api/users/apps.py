from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'podcast_system.api.users'
