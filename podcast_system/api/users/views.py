from django.contrib.auth import login, logout 
from django.contrib.auth.forms import AuthenticationForm # new
from rest_framework import generics, permissions, renderers, viewsets,status, views
from rest_framework.views import APIView
from rest_framework.response import Response
from podcast_system.api.users.serializers import UserSerializer, ProfileSerializer
from podcast_system.api.users.models import User, Profile





class UserViewSet(viewsets.ModelViewSet):
    """
    Updates and retrieves user accounts
    """
    lookup_field = 'username'
    queryset = User.objects.all()
    serializer_class = UserSerializer 
  #  permission_classes = (DRYObjectPermissions,)

class LogInView(APIView):
    def post(self, request):
        form = AuthenticationForm(data=request.data)
        if form.is_valid():
            user = form.get_user()
            login(request, user=form.get_user())
            serializer = UserSerializer(user,context={'request': request})
            return Response(serializer.data)
        else:
            return Response(form.errors, status=status.HTTP_400_BAD_REQUEST)


# new
class LogOutView(APIView):
   # permission_classes = (permissions.IsAuthenticated,)

    def post(self, *args, **kwargs):
        logout(self.request)
        return Response(status=status.HTTP_204_NO_CONTENT)

class UserProfileViewSet(generics.RetrieveUpdateAPIView, viewsets.GenericViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer