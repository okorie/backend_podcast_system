from django.conf.urls import include, url
from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns


from rest_framework.routers import SimpleRouter

from podcast_system.api.users import views

# Create a router and register our viewsets with it.


router = SimpleRouter()
router.register(r'users', views.UserViewSet)
router.register(r'profiles', views.UserProfileViewSet)

urlpatterns = [
    url('login/', views.LogInView.as_view()), # new
    url('logout/', views.LogOutView.as_view()), # new
]

urlpatterns += router.urls


# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.
