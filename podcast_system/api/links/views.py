from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .permissions import IsOwnerOrAdminOrReadOnly
from podcast_system.api.links.models import Podcast_link
from podcast_system.api.links.serializers import PodcastSerializer
import feedparser
from podcast_system.api.users.models import User



class PodcastLinkViewset(viewsets.ModelViewSet):
    queryset = Podcast_link.objects.all()
    serializer_class = PodcastSerializer
    permission_classes = [IsOwnerOrAdminOrReadOnly]




@api_view(['GET'])
def FeedsItems(request, username=None):
    feeds = Podcast_link.objects.filter(author__username = username)
    items = []
    for feed in feeds:
        rss = feedparser.parse(feed.link)
        items = rss["items"]

        try:
            items.extend(items)
        except KeyError:
                continue

    items = list(reversed(sorted(items, key=lambda item: item["published_parsed"])))
    items = Response(items)
    return items
