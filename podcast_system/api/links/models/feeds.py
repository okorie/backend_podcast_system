import uuid
from django.db import models
from django.conf import settings

# Create your models here.


class Podcast_link(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    link = models.URLField(max_length=255, unique=False)
    def __str__(self):
        return "<Feed '{}'>".format(self.link)

    @property
    def author_name(self):
        return self.author.get_full_name()
