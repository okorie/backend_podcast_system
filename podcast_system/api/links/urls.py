from django.urls import path, re_path
from rest_framework.routers import SimpleRouter
from django.conf.urls import include, url
from podcast_system.api.links import views


router = SimpleRouter()
router.register(r'podcast', views.PodcastLinkViewset)

urlpatterns = [
    url('^feeds/(?P<username>.+)/$', views.FeedsItems),

]

urlpatterns += router.urls

