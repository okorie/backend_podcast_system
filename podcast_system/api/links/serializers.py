from rest_framework import serializers
from podcast_system.api.links.models import Podcast_link

class PodcastSerializer(serializers.ModelSerializer):
    
    """
    create links.
    """

    def create(self, validated_data):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            validated_data['author'] = user

        podcast_link = Podcast_link(**validated_data)
        podcast_link.save()
        return podcast_link

    class Meta:
        model = Podcast_link
        fields = ['link','author', 'author_name', 'id']
