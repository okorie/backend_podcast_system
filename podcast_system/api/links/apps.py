from django.apps import AppConfig


class LinksConfig(AppConfig):
    name = 'podcast_system.api.links'
