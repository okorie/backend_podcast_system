from django.urls import include, path

urlpatterns = [
    path('', include('podcast_system.api.users.urls')),
    path('', include('podcast_system.api.links.urls')),

    
    # Your stuff: custom urls includes go here
] 